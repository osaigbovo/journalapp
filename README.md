# JournalApp

Here is a fully functional and colorful android app which I made from scratch for Andela-Google-Udacity Android Developer Scholarship. 
This app reveals the power of Firebase, Dagger, and adaptive UI both for phone and tablet devices.

# Features
With the app, you can:

Write down how you day went, putting donw precious thought to a secure archive.
Modify and delete your journal entries.
See the date you filled in an entry.
This app requires a Gmail account for login.

# Screens
*Signin Screen*
![picture alt](https://github.com/Osaigbovo/JournalApp/blob/master/art/SignInScreen.jpg "Signin Screen")

*Home Screen*
![picture alt](https://github.com/Osaigbovo/JournalApp/blob/master/art/HomeScreen.jpg "Home Screenl")

*HomeEdit Screen*
![picture alt](https://github.com/Osaigbovo/JournalApp/blob/master/art/HomeEditScreen.jpg "Modify PopUp")

*Calender Screen*
![picture alt](https://github.com/Osaigbovo/JournalApp/blob/master/art/CalenderScreen.jpg "Calender Screen")

*User Screen*
![picture alt](https://github.com/Osaigbovo/JournalApp/blob/master/art/UserScreen.jpg "User Info Screenl")

# Libraries
* Dagger
* Firebase
* Glide
* ViewModel, LiveData and Lifecycle
* MCalenderView
* Espresso & AndroidJUnitRunner


# License
Copyright 2018 Osaigbovo Odiase

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
